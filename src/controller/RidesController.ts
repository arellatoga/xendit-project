'use strict';

import 'reflect-metadata';
import {injectable, singleton} from 'tsyringe';

import RidesService from '../service/RidesService';
import * as express from 'express';

import {Errors} from '../model/Errors';
import {Ride} from '../model/Ride';
import {Error} from '../model/Error';
import {logger} from '../util/Logger';

const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

@singleton()
@injectable()
export default class RidesController {
  constructor(public ridesService: RidesService) {
  }
  public routes(app: any): void {
    app.post('/rides', jsonParser,
        (req: express.Request, res: express.Response) => {
          logger.info(`${req.method} ${req.url} ${JSON.stringify(req.body)}`);
          const ride: Ride = {
            startLat: req.body.start_lat,
            startLong: req.body.start_long,
            endLat: req.body.end_lat,
            endLong: req.body.end_long,
            riderName: req.body.rider_name,
            driverName: req.body.driver_name,
            driverVehicle: req.body.driver_vehicle,
          };

          if (!isValidRide(ride)) {
            return res.status(Errors.INVALID_REQUEST.statusCode)
                .send(Errors.INVALID_REQUEST.cause);
          } else {
            this.ridesService.newRide(ride)
                .then((rows) => {
                  return res.status(201).send({'created': rows});
                }).catch((error: Error) => {
                  return res.status(error.statusCode).send(error.cause);
                });
          }
        });

    app.get('/rides', (req: express.Request, res: express.Response) => {
      logRequest(req);
      const page:number = Number(req.query.page) || 0;
      this.ridesService.getAllRides(page)
          .then((rides) => {
            return res.status(200).send({'rides': rides});
          }).catch((error: Error) => {
            return res.status(error.statusCode).send(error.cause);
          });
    });

    app.get('/rides/:id', (req: express.Request, res: express.Response) => {
      logRequest(req);
      const id = Number(req.params.id);

      if (isNaN(id)) {
        return res.status(Errors.INVALID_REQUEST.statusCode)
            .send(Errors.INVALID_REQUEST.cause);
      }

      this.ridesService.getRideById(id)
          .then((ride) => {
            return res.status(200).send({'ride': ride});
          }).catch((error: Error) => {
            return res.status(error.statusCode).send(error.cause);
          });
    });

    function logRequest(req: express.Request) {
      logger.info(`${req.method} ${req.url}`);
    }

    function isValidRide(ride: Ride): Boolean {
      if (isNaN(ride.startLat) ||
            isNaN(ride.startLong) ||
            isNaN(ride.endLat) ||
            isNaN(ride.endLong) ||
            !ride.riderName ||
            !ride.driverName ||
            !ride.driverVehicle) {
        return false;
      }

      return true;
    }
  }
}


// module.exports = router;
