export interface ErrorObject {
    errorCode: string;
    message: string;
}
export interface Error {
    statusCode: number;
    cause: ErrorObject;
}
