import type {Error} from 'model/Error';

export abstract class Errors {
    public static INVALID_REQUEST: Error = {
      statusCode: 400,
      cause: {
        errorCode: 'REQUEST_ERROR',
        message: 'Request is invalid',
      },
    }
    public static INVALID_MIN_COORDS: Error = {
      statusCode: 400,
      cause: {
        errorCode: 'VALIDATION_ERROR',
        message: 'Start latitude and longitude must be between' +
            '-90 to 90 and -180 to 180 degrees respectively',
      },
    }
    public static INVALID_MAX_COORDS: Error = {
      statusCode: 400,
      cause: {
        errorCode: 'VALIDATION_ERROR',
        message: 'End latitude and longitude must be between' +
            '-90 to 90 and -180 to 180 degrees respectively',
      },
    }
    public static INVALID_RIDER_NAME: Error = {
      statusCode: 400,
      cause: {
        errorCode: 'VALIDATION_ERROR',
        message: 'Rider name must be a non empty string',
      },
    };
    public static INVALID_DRIVER_NAME: Error = {
      statusCode: 400,
      cause: {
        errorCode: 'VALIDATION_ERROR',
        message: 'Driver name must be a non empty string',
      },
    };
    public static INVALID_VEHICLE_NAME: Error = {
      statusCode: 400,
      cause: {
        errorCode: 'VALIDATION_ERROR',
        message: 'Vehicle name must be a non empty string',
      },
    };
    public static RIDE_NOT_FOUND: Error = {
      statusCode: 400,
      cause: {
        errorCode: 'RIDES_NOT_FOUND_ERROR',
        message: 'Could not find any rides',
      },
    };
    public static DATABASE_ERROR: Error = {
      statusCode: 500,
      cause: {
        errorCode: 'DATABASE_ERROR',
        message: 'Database is down',
      },
    };
}
