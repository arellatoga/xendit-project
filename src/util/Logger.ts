import * as winston from 'winston';

export const logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.printf((info) =>
        `[${info.timestamp}] [${info.level}] ${info.message}`),
  ),
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({filename: 'application.log', level: 'info'}),
    new winston.transports.File({filename: 'error.log', level: 'error'}),
  ],
});
winston.loggers.add('rides-logger', logger);
