import 'reflect-metadata';

import {singleton} from 'tsyringe';
import {Ride} from '../model/Ride';
import {Errors} from '../model/Errors';
import * as sqlite from 'sqlite3';
import {Config} from '../../config/Config';

import {logger} from '../util/Logger';
import RepositoryInterface from './RepositoryInterface';

const sql = sqlite.verbose();

@singleton()
export default class Repository implements RepositoryInterface {
    db: sqlite.Database;

    constructor() {
      const filename: string = Config.db.databaseFileName;
      this.loadFile(filename);
    }

    public closeDatabase() {
      if (!!this.db) {
        this.db.close((err: any) => {
          if (err) {
            logger.error(`error encountered while closing database: ${err}`);
          } else {
            logger.info(`Successfully closed database`);
          }
        });
      }
    }

    public loadFile(filename: string) {
      this.closeDatabase();
      logger.log({level: 'info', message: `Opening database file ${filename}`});
      const newDb = new sql.Database(filename, (err: any) => {
        if (err) {
          logger.info(`error encountered while connecting to database: ${err}`);
        } else {
          logger.info(`Successfully connected to database`);
        }
      });
      newDb.run(Config.db.createTableSchema, (err: any) => {
        if (err) {
          logger.info(`error encountered while creating database: ${err}`);
        } else {
          logger.info(`Successfully set up database`);
        }
      });

      this.db = newDb;
    }

    private static convertObject(ride?: any): Ride {
      if (ride == null) {
        return null;
      } else {
        const converted: Ride = {
          id: ride.rideID,
          startLat: ride.startLat,
          startLong: ride.startLong,
          endLat: ride.endLat,
          endLong: ride.endLong,
          riderName: ride.riderName,
          driverName: ride.driverName,
          driverVehicle: ride.driverVehicle,
          createDate: ride.created,
        };

        return converted;
      }
    }

    public insert(ride: Ride): Promise<Ride> {
      logger.log('info', `Inserting into database ${JSON.stringify(ride)}`);
      return new Promise<Number>((resolve, reject) => {
        const values = [ride.startLat, ride.startLong,
          ride.endLat, ride.endLong,
          ride.riderName, ride.driverName, ride.driverVehicle];
        this.db.run(Config.db.insertQuery, values, function(err: any) {
          if (err) {
            logger.error(`Error encountered: ${err}`);
            reject(Errors.DATABASE_ERROR);
          } else {
            logger.info(`Inserted into database with id ${this.lastID}`);
            resolve(this.lastID);
          }
        });
      }).then((result) => {
        return this.find(result);
      });
    }

    public find(id: Number): Promise<Ride> {
      return new Promise<Ride>((resolve, reject) => {
        this.db.all(Config.db.getWithIdQuery, id, (err: any, rows: any[]) => {
          if (err) {
            logger.error(`Error encountered: ${err}`);
            reject(Errors.DATABASE_ERROR);
          } else {
            const ride: Ride = Repository.convertObject(rows[0]);
            if (!!ride) {
              logger.info(`Found ride record ${JSON.stringify(rows[0])}`);
            } else {
              logger.info(`No found ride with given id ${id}`);
            }
            resolve(ride);
          }
        });
      });
    }

    public findAll(page: number = 0): Promise<Array<Ride>> {
      return new Promise<Ride[]>((resolve, reject) => {
        const count = 10;
        const offset = count * page;
        this.db.all(Config.db.getAllQuery, [count, offset],
            (err: any, entries: any[]) => {
              if (err) {
                logger.error(`Error encountered: ${err}`);
                reject(Errors.DATABASE_ERROR);
              } else {
                const rides: Array<Ride> = [];
                for (let i = 0; i < entries.length; i++) {
                  const ride: Ride = Repository.convertObject(entries[i]);
                  rides.push(ride);
                }
                logger.info(`Found ride records ${JSON.stringify(rides)}`);
                resolve(rides);
              }
            });
      });
    }
}
