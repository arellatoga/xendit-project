'use strict';

import 'reflect-metadata';
import {injectable, singleton} from 'tsyringe';
import {Ride} from 'model/Ride';
import Repository from '../repository/Repository';
import {Errors} from '../model/Errors';

@singleton()
@injectable()
export default class RidesService {
  constructor(public repository: Repository) {
  }

  validateRide(ride: Ride): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      if (ride.startLat < -90 || ride.startLat > 90 ||
          ride.startLong < -180 || ride.startLong > 180) {
        return reject(Errors.INVALID_MIN_COORDS);
      } else if (ride.endLat < -90 || ride.endLat > 90 ||
          ride.endLong < -180 || ride.endLong > 180) {
        return reject(Errors.INVALID_MAX_COORDS);
      } else if (typeof ride.riderName !== 'string' ||
          ride.riderName.length < 1) {
        return reject(Errors.INVALID_RIDER_NAME);
      } else if (typeof ride.driverName !== 'string' ||
          ride.driverName.length < 1) {
        return reject(Errors.INVALID_DRIVER_NAME);
      } else if (typeof ride.driverVehicle !== 'string' ||
          ride.driverVehicle.length < 1) {
        return reject(Errors.INVALID_VEHICLE_NAME);
      } else {
        return resolve();
      }
    });
  }

  newRide(ride: Ride): Promise<Ride> {
    return this.validateRide(ride).then(() => {
      return this.repository.insert(ride);
    });
  }

  getAllRides(page: number = 0): Promise<Array<Ride>> {
    return this.repository.findAll(page).then((rideModels: Array<Ride>) => {
      return new Promise<Array<Ride>>((resolve, reject) => {
        if (rideModels.length == 0) {
          reject(Errors.RIDE_NOT_FOUND);
        } else {
          resolve(rideModels);
        }
      });
    });
  }

  getRideById(id: Number): Promise<Ride> {
    return this.repository.find(id).then((ride: Ride) => {
      return new Promise<Ride>((resolve, reject) => {
        if (ride == null) {
          reject(Errors.RIDE_NOT_FOUND);
        } else {
          resolve(ride);
        }
      });
    });
  }
}
