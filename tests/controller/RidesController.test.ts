import "reflect-metadata";
import * as request  from "supertest"
import * as express from "express"
import RidesController from "../../src/controller/RidesController";
import {Ride} from "../../src/model/Ride";
import {Response} from "supertest";
import {container, injectable, singleton} from "tsyringe";
import RidesService from "../../src/service/RidesService";
import Repository from "../../src/repository/Repository";
import {Errors} from "../../src/model/Errors";

describe('RidesController', function() {
    let app = express();
    let controllerUnderTest: RidesController;
    let rideArray: Array<Ride> = [];
    let ride1: Ride;
    let ride2: Ride;

    let getRideMock = jest.fn();
    let getAllMock = jest.fn();
    let newRideMock = jest.fn();

    @singleton()
    @injectable()
    class RidesServiceFake {
        constructor(public repository: Repository) {
        }
        getAllRides():Promise<Array<Ride>> {
            return new getAllMock();
        }
        newRide(ride: Ride):Promise<Ride> {
            return new newRideMock();
        }
        getRideById(id: Number):Promise<Ride> {
            return new getRideMock();
        }
    }

    function setupRides() {
        ride1 = {
            id: 1,
            startLat: 100,
            startLong: 100,
            endLat: 100,
            endLong: 100,
            riderName: "rider",
            driverName: "driver",
            driverVehicle: "vehicle"
        }
        ride2 = {
            id: 2,
            startLat: 100,
            startLong: 100,
            endLat: 100,
            endLong: 100,
            riderName: "rider2",
            driverName: "driver2",
            driverVehicle: "vehicle2"
        }

        rideArray.push(ride1);
        rideArray.push(ride2);
    }

    beforeEach(() => {
        setupRides();
        container.reset()
        container.clearInstances();
        container.register<RidesService>(RidesService, RidesServiceFake);
        controllerUnderTest = container.resolve(RidesController);
        controllerUnderTest.routes(app);
    });

    afterEach(function() {
        jest.resetAllMocks();
    });

    describe('GET /rides', function() {
        it('returns all rides', function() {
            getAllMock.mockImplementation(() => Promise.resolve(rideArray));
            return request(app).get("/rides").then((response: Response) => {
                let responseMap = JSON.parse(JSON.stringify(response.body));
                expect(responseMap.rides[0]).toEqual(ride1);
                expect(responseMap.rides[1]).toEqual(ride2);
            });
        });

        describe('returns errors', function() {
            it('when the service returns an error', function() {
                getAllMock.mockImplementation(() => Promise.reject(Errors.RIDE_NOT_FOUND));
                return request(app).get("/rides").then((response: Response) => {
                    expect(response.body).toEqual(Errors.RIDE_NOT_FOUND.cause);
                    expect(response.status).toEqual(Errors.RIDE_NOT_FOUND.statusCode);
                });
            });
        });
    });

    describe('GET /rides/:id', function() {
        it('returns a single ride', function() {
            getRideMock.mockImplementation(() => Promise.resolve(ride1));
            let rideId = 1;
            return request(app).get(`/rides/${rideId}`).then((response: Response) => {
                let responseBody = JSON.parse(JSON.stringify(response.body));
                expect(responseBody.ride).toEqual(ride1);
            });
        });

        describe('returns errors', function() {
            it('when the service returns an error', function() {
                getRideMock.mockImplementation(() => Promise.reject(Errors.RIDE_NOT_FOUND));
                let rideId = 99;
                return request(app).get(`/rides/${rideId}`).then((response: Response) => {
                    expect(response.status).toEqual(Errors.RIDE_NOT_FOUND.statusCode);
                    expect(response.body).toEqual(Errors.RIDE_NOT_FOUND.cause);
                });
            });

            it('when the specified id is not a number', function() {
                let id = "test";
                return request(app).get(`/rides/${id}`).then((response: Response) => {
                    expect(response.body).toEqual(Errors.INVALID_REQUEST.cause);
                    expect(response.status).toEqual(Errors.INVALID_REQUEST.statusCode);
                });
            });
        });
    });

    describe('POST /rides', function() {
        let reqBody = {
            start_lat: 100,
            start_long: 100,
            end_lat: 100,
            end_long: 100,
            rider_name: "rider",
            driver_name: "driver",
            driver_vehicle: "vehicle"
        };

        it('creates a ride entry in the database', function() {
            let reqBody = {
                start_lat: 100,
                start_long: 100,
                end_lat: 100,
                end_long: 100,
                rider_name: "rider",
                driver_name: "driver",
                driver_vehicle: "vehicle"
            };
            newRideMock.mockImplementation(() => Promise.resolve(ride1));
            return request(app).post('/rides')
                .send(reqBody)
                .then((response) => {
                    let responseBody = JSON.parse(JSON.stringify(response.body));
                    expect(response.status).toEqual(201);
                    expect(responseBody.created).toEqual(ride1);
            });
        });

        describe('returns an error', function () {
            it('on invalid request bodies', function() {
                let reqBody = {
                    start_lat: "hello",
                    start_long: "i",
                    end_lat: "am",
                    end_long: "invalid",
                    rider_name: "rider",
                    driver_name: "driver",
                    driver_vehicle: "vehicle"
                };
                newRideMock.mockImplementation(() => Promise.resolve(ride1));
                return request(app).post('/rides')
                    .send(reqBody)
                    .then((response) => {
                        expect(response.status).toEqual(Errors.INVALID_REQUEST.statusCode);
                        expect(response.body).toEqual(Errors.INVALID_REQUEST.cause);
                    });
            });

            it('when the service returns an error', function() {
                let reqBody = {
                    start_lat: 100,
                    start_long: 100,
                    end_lat: 100,
                    end_long: 100,
                    rider_name: "rider",
                    driver_name: "driver",
                    driver_vehicle: "vehicle"
                };
                newRideMock.mockImplementation(() => Promise.reject(Errors.DATABASE_ERROR));
                return request(app).post('/rides')
                    .send(reqBody)
                    .then((response) => {
                        expect(response.status).toEqual(Errors.DATABASE_ERROR.statusCode);
                        expect(response.body).toEqual(Errors.DATABASE_ERROR.cause);
                    });
            });
        });
    });
});