import "reflect-metadata";
import {Ride} from "../../src/model/Ride";
import RidesService from "../../src/service/RidesService";
import Repository from "../../src/repository/Repository";
import SpyInstance = jest.SpyInstance;
import { Config } from "../../config/Config"
import {Errors} from "../../src/model/Errors";
import exp = require("constants");
import * as fs from "fs";

const FILENAME: string = "file::memory:?cache=shared";

describe('Repository', function() {
    let serviceUnderTest: Repository;
    let rideArray: Array<Ride> = [];
    let ride1: Ride;
    let ride2: Ride;

    let dummyDate = new Date();

    function setupRides() {
        ride1 = {
            id: 1,
            startLat: 60,
            startLong: 60,
            endLat: 60,
            endLong: 60,
            riderName: "rider",
            driverName: "driver",
            driverVehicle: "vehicle",
            createDate: dummyDate
        }
        ride2 = {
            id: 2,
            startLat: 60,
            startLong: 60,
            endLat: 60,
            endLong: 60,
            riderName: "rider2",
            driverName: "driver2",
            driverVehicle: "vehicle2"
        }

        rideArray.push(ride1);
        rideArray.push(ride2);
    }

    beforeEach((done) => {
        setupRides();
        serviceUnderTest = new Repository();
        serviceUnderTest.closeDatabase();
        serviceUnderTest.loadFile(FILENAME);

        done();
    });

    afterEach(function(done) {
        serviceUnderTest.closeDatabase();
        done();
    });

    afterAll(function (done) {
        fs.unlinkSync(FILENAME);
        done();
    })

    describe('insert()', function() {
        xit('should insert ride', function(done) {
            return serviceUnderTest.insert(ride1).then((expected: Ride) => {
                return serviceUnderTest.find(expected.id)
            }).then(actual => {
                expect(actual.riderName).toEqual(ride1.riderName);
                expect(actual.driverVehicle).toEqual(ride1.driverVehicle);
                expect(actual.driverName).toEqual(ride1.driverName);
                done()
            });
        });

        describe('returns an error', function () {
            it('when database is unavailable', (done) => {
                serviceUnderTest.closeDatabase();
                return serviceUnderTest.insert(ride1).catch(err => {
                    expect(err).toEqual(Errors.DATABASE_ERROR);
                    done();
                })
            });
        });
    });

    describe('find()', function() {
        it('should find a ride with the id', function(done) {
            return serviceUnderTest.insert(ride1).then((expected: Ride) => {
                return serviceUnderTest.find(expected.id).then(actual => {
                    expect(actual.riderName).toEqual(expected.riderName);
                    expect(actual.driverVehicle).toEqual(expected.driverVehicle);
                    expect(actual.driverName).toEqual(expected.driverName);
                    done()
                });
            });
        });

        it('should return a null value for nonexistent rides', function(done) {
            return serviceUnderTest.find(99).then(actual => {
                expect(actual).toEqual(null);
                done()
            });
        });

        describe('returns an error', function () {
            it('when database is unavailable', (done) => {
                serviceUnderTest.closeDatabase();
                return serviceUnderTest.find(1).catch(err => {
                    expect(err).toEqual(Errors.DATABASE_ERROR);
                    done();
                })
            });
        });
    });

    describe('findAll()', function() {
        beforeEach(done => {
            fs.unlinkSync(FILENAME);
            serviceUnderTest = new Repository();
            serviceUnderTest.closeDatabase();
            serviceUnderTest.loadFile(FILENAME);
            done();
        });

        xit('should find all rides', function(done) {
            return serviceUnderTest.insert(ride1).then((expected1: Ride) => {
                return serviceUnderTest.insert(ride2).then((expected2: Ride) => {
                    return serviceUnderTest.findAll().then(actual => {
                        expect(actual).toEqual([ expected1, expected2 ]);
                        done()
                    });
                });
            });
        });

        xit('should return a null value for nonexistent rides', function(done) {
            return serviceUnderTest.findAll().then(actual => {
                expect(actual).toEqual([]);
                done()
            });
        });

        describe('returns an error', function () {
            it('when database is unavailable', (done) => {
                serviceUnderTest.closeDatabase();
                return serviceUnderTest.findAll().catch(err => {
                    expect(err).toEqual(Errors.DATABASE_ERROR);
                    done();
                })
            });
        });
    });
});