import "reflect-metadata";
import {Ride} from "../../src/model/Ride";
import {container, injectable, singleton} from "tsyringe";
import {Errors} from "../../src/model/Errors";
import RidesService from "../../src/service/RidesService";
import Repository from "../../src/repository/Repository";
import * as sqlite from 'sqlite3';
import RepositoryInterface from "../../src/repository/RepositoryInterface";

describe('RidesService', function() {
    let serviceUnderTest: RidesService;
    let rideArray: Array<Ride> = [];
    let ride1: Ride;
    let ride2: Ride;

    let findMock = jest.fn();
    let findAllMock = jest.fn();
    let insertMock = jest.fn();

    @singleton()
    class RepositoryFake implements RepositoryInterface {
        db: sqlite.Database;

        constructor() {
        }

        findAll():Promise<Array<Ride>> {
            return new findAllMock();
        }
        find(id: Number):Promise<Ride> {
            return new findMock();
        }
        insert(ride: Ride):Promise<Ride> {
            return new insertMock();
        }
        closeDatabase() {
        }
        loadFile() {
        }
    }

    function setupRides() {
        ride1 = {
            id: 1,
            startLat: 60,
            startLong: 60,
            endLat: 60,
            endLong: 60,
            riderName: "rider",
            driverName: "driver",
            driverVehicle: "vehicle"
        }
        ride2 = {
            id: 2,
            startLat: 60,
            startLong: 60,
            endLat: 60,
            endLong: 60,
            riderName: "rider2",
            driverName: "driver2",
            driverVehicle: "vehicle2"
        }

        rideArray.push(ride1);
        rideArray.push(ride2);
    }

    beforeEach(() => {
        setupRides();
        container.reset()
        container.clearInstances();
        container.register<Repository>(Repository, RepositoryFake);
        serviceUnderTest = container.resolve(RidesService);
    });

    afterEach(function() {
        jest.resetAllMocks();
    });

    describe('getRideById()', function() {
        it('returns ride with specified id', function() {
            let id = 1;
            findMock.mockImplementation(() => Promise.resolve(ride1));
            serviceUnderTest.getRideById(1).then((actual) => {
                expect(actual).toEqual(ride1);
            });
        });

        describe('returns errors', function() {
            it('when the repository returns an error', function() {
                let id = 1;
                findMock.mockImplementation(() => Promise.reject(Errors.DATABASE_ERROR));
                serviceUnderTest.getRideById(1).catch((error) => {
                    expect(error).toEqual(Errors.DATABASE_ERROR);
                });
            });

            it('when there are no rides to be found', function() {
                findMock.mockImplementation(() => Promise.resolve(null));
                serviceUnderTest.getRideById(1).catch((error: Error) => {
                    expect(error).toEqual(Errors.RIDE_NOT_FOUND);
                });
            });
        });
    });

    describe('getAllRides()', function() {
        it('returns all rides', function() {
            findAllMock.mockImplementation(() => Promise.resolve(rideArray));
            serviceUnderTest.getAllRides().then((actual) => {
                expect(actual).toEqual(rideArray);
            });
        });

        describe('returns errors', function() {
            it('when the repository returns an error', function() {
                findAllMock.mockImplementation(() => Promise.reject(Errors.DATABASE_ERROR));
                serviceUnderTest.getAllRides().catch((error) => {
                    expect(error).toEqual(Errors.DATABASE_ERROR);
                });
            });

            it('when there are no rides to be found', function() {
                findAllMock.mockImplementation(() => Promise.resolve([]));
                serviceUnderTest.getAllRides().catch((error: Error) => {
                    expect(error).toEqual(Errors.RIDE_NOT_FOUND);
                });
            });
        });
    });

    describe('newRide()', function() {
        it('returns ride with specified id', function() {
            insertMock.mockImplementation(() => Promise.resolve(ride1));
            serviceUnderTest.newRide(ride1).then((actual) => {
                expect(actual).toEqual(ride1);
            });
        });

        describe('returns errors', function() {
            it('when the repository returns an error', function() {
                findMock.mockImplementation(() => Promise.reject(Errors.DATABASE_ERROR));
                serviceUnderTest.newRide(ride1).catch((error) => {
                    expect(error).toEqual(Errors.DATABASE_ERROR);
                });
            });

            it('when start_lat is invalid', function() {
                let ride3 = ride1;
                ride3.startLat = 999;
                serviceUnderTest.newRide(ride3).catch((error) => {
                    expect(error).toEqual(Errors.INVALID_MIN_COORDS);
                });
            });

            it('when start_long is invalid', function() {
                let ride3 = ride1;
                ride3.startLong = 999;
                serviceUnderTest.newRide(ride3).catch((error) => {
                    expect(error).toEqual(Errors.INVALID_MIN_COORDS);
                });
            });

            it('when end_lat is invalid', function() {
                let ride3 = ride1;
                ride3.endLat = 999;
                serviceUnderTest.newRide(ride3).catch((error) => {
                    expect(error).toEqual(Errors.INVALID_MAX_COORDS);
                });
            });

            it('when end_long is invalid', function() {
                let ride3 = ride1;
                ride3.endLong = 999;
                serviceUnderTest.newRide(ride3).catch((error) => {
                    expect(error).toEqual(Errors.INVALID_MAX_COORDS);
                });
            });


            it('when rider_name is invalid', function() {
                let ride3 = ride1;
                ride3.riderName = "";
                serviceUnderTest.newRide(ride3).catch((error) => {
                    expect(error).toEqual(Errors.INVALID_RIDER_NAME);
                });
            });

            it('when driver_name is invalid', function() {
                let ride3 = ride1;
                ride3.driverName = "";
                serviceUnderTest.newRide(ride3).catch((error) => {
                    expect(error).toEqual(Errors.INVALID_DRIVER_NAME);
                });
            });

            it('when driver_vehicle is invalid', function() {
                let ride3 = ride1;
                ride3.driverVehicle = "";
                serviceUnderTest.newRide(ride3).catch((error) => {
                    expect(error).toEqual(Errors.INVALID_VEHICLE_NAME);
                });
            });
        });
    });
});