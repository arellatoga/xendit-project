export abstract class Config {
    public static db = {
        "createTableSchema": "CREATE TABLE IF NOT EXISTS Rides(rideID INTEGER PRIMARY KEY AUTOINCREMENT,startLat DECIMAL NOT NULL,startLong DECIMAL NOT NULL,endLat DECIMAL NOT NULL,endLong DECIMAL NOT NULL,riderName TEXT NOT NULL,driverName TEXT NOT NULL,driverVehicle TEXT NOT NULL,created DATETIME default CURRENT_TIMESTAMP)",
        "insertQuery": "INSERT INTO Rides(startLat, startLong, endLat, endLong, riderName, driverName, driverVehicle) VALUES (?, ?, ?, ?, ?, ?, ?)",
        "getWithIdQuery": "SELECT * FROM Rides WHERE rideID = ?",
        "getAllQuery": "SELECT * FROM Rides LIMIT ? OFFSET ?",
        "databaseFileName": "database.db"
    }
    public static server = {
        "port": 8080
    }
}