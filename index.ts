import * as express from "express";
import * as winston from 'winston';
import "reflect-metadata";

import RidesController from "./src/controller/RidesController";
import {container} from "tsyringe";
import {Config} from "./config/Config";

const app = express();
const port = Config.server.port;

winston.loggers.add('logger', {
    level: 'info',
    format: winston.format.json(),
    transports: [
        new winston.transports.Console()
    ]
})

const ridesController = container.resolve(RidesController);
ridesController.routes(app)
app.listen(port, () => console.log(`App started and listening on port ${port}`));
